$(function () {
    $('form').on('submit', function () {
        var name = $("#name").val();
        var lastName = $("#last_name").val();
        var sex = $("#sex").val();

        if (name !== '' && lastName !== '' && sex !== ''){
            $.ajax({
                type: 'post',
                url: 'submit.php',
                data: {
                    name: name,
                    last_name: lastName,
                    sex: sex
                },
                success: function (response) {
                    alert(response);
                }
            });
            return false;
        } else {
            alert('Заполните все поля!');
        }
    });
});
