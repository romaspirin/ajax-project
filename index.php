<html>
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="script.js"></script>
</head>
<body>
    <h1>Заполните форму</h1>
    <div class="main">
        <form id="form" action="" method="POST">
            <div class="field">
                <p>
                    <label>Имя:</label>&nbsp;
                    <input type="text" name="name" id="name">
                </p>
            </div>
            <div class="field">
                <p>
                    <label>Фамилия:</label>&nbsp;
                    <input type="text" name="last_name" id="last_name">
                </p>
            </div>
            <div class="field">
                <p>
                    <label>Пол:</label>&nbsp;
                    <select name="sex" id="sex">
                        <option value="">Ваш пол</option>
                        <option value="male">Мужской</option>
                        <option value="female">Женский</option>
                    </select>
                </p>
            </div>
            <p><input id="submit" type="submit" value="Отправить"></p>
        </form>
        <br />
        <p>все поля обязательны для заполнения</p>
    </div>
</body>
</html>
