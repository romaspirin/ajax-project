<?php

$users = [
    [
        'name' => 'Рома',
        'last_name' => 'Спирин',
        'sex' => 'male'
    ],
    [
        'name' => 'Иван',
        'last_name' => 'Иванов',
        'sex' => 'female'
    ]
];

if (isset($_POST['name'])){
    $user = [];
    $user['name'] = trim(htmlspecialchars($_POST['name']));
    $user['last_name'] = trim(htmlspecialchars($_POST['last_name']));
    $user['sex'] = $_POST['sex'];

    $status = [];
    foreach ($users as $oneUser){
        if ($oneUser['name'] == $user['name'] &&
            $oneUser['last_name'] == $user['last_name'] &&
            $oneUser['sex'] == $user['sex']){
            $status[] = true;
        } else {
            $status[] = false;
        }
    }

    if(in_array(true, $status)){
        echo "Exists";
    } else {
        echo "Created";
    }
}
?>
